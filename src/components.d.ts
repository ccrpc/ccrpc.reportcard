/* eslint-disable */
/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */
import { HTMLStencilElement, JSXBase } from "@stencil/core/internal";
export namespace Components {
    interface ReportcardPerformanceMeasureTable {
        /**
          * The API URL for the performance measure
         */
        "url": string;
    }
    interface ReportcardSummaryTable {
        "url": string;
    }
}
declare global {
    interface HTMLReportcardPerformanceMeasureTableElement extends Components.ReportcardPerformanceMeasureTable, HTMLStencilElement {
    }
    var HTMLReportcardPerformanceMeasureTableElement: {
        prototype: HTMLReportcardPerformanceMeasureTableElement;
        new (): HTMLReportcardPerformanceMeasureTableElement;
    };
    interface HTMLReportcardSummaryTableElement extends Components.ReportcardSummaryTable, HTMLStencilElement {
    }
    var HTMLReportcardSummaryTableElement: {
        prototype: HTMLReportcardSummaryTableElement;
        new (): HTMLReportcardSummaryTableElement;
    };
    interface HTMLElementTagNameMap {
        "reportcard-performance-measure-table": HTMLReportcardPerformanceMeasureTableElement;
        "reportcard-summary-table": HTMLReportcardSummaryTableElement;
    }
}
declare namespace LocalJSX {
    interface ReportcardPerformanceMeasureTable {
        /**
          * The API URL for the performance measure
         */
        "url"?: string;
    }
    interface ReportcardSummaryTable {
        "url"?: string;
    }
    interface IntrinsicElements {
        "reportcard-performance-measure-table": ReportcardPerformanceMeasureTable;
        "reportcard-summary-table": ReportcardSummaryTable;
    }
}
export { LocalJSX as JSX };
declare module "@stencil/core" {
    export namespace JSX {
        interface IntrinsicElements {
            "reportcard-performance-measure-table": LocalJSX.ReportcardPerformanceMeasureTable & JSXBase.HTMLAttributes<HTMLReportcardPerformanceMeasureTableElement>;
            "reportcard-summary-table": LocalJSX.ReportcardSummaryTable & JSXBase.HTMLAttributes<HTMLReportcardSummaryTableElement>;
        }
    }
}
