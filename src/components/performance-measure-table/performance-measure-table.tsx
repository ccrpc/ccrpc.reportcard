import { Component, Prop, State, h, getAssetPath } from '@stencil/core';


@Component({
  tag: 'reportcard-performance-measure-table',
  styleUrl: 'table.css',
  shadow: true,
  assetsDirs: ['rating-image']
})
export class PerformanceMeasureTable {

  // Indicate that name should be a public property on the component
  /**
  * The API URL for the performance measure
  */
  @Prop() url: string;
  /**
  * JSON returned by the API that contains attributes of the report card,
  * goal, performance measure, and annual ratings.
  *  format: {
  *              "id":...,
  *              "goal":...,
  *              "title":...,
  *              "objective":...,
  *              "description":...,
  *              "ratings":[
  *              {rating1},{rating2},{rating3}
  *              ]
  *            }
  *
  * For more information, see url in Embed Performance Measure Table section
  * of the report card editing interface
  */
  @State() pmData: any;

  async componentWillLoad() {
    let response = await fetch(this.url);
    this.pmData = await response.json();
  }

  render() {
    let {goal, title, objective, description, ratings} = this.pmData;

    let rows = ratings.map((rating, i) => {
      let image = (rating.rating === 'baseline') ? null :
        <img src={getAssetPath(`./rating-image/${rating.rating}.png`)}
          alt={rating.rating} />;
      let rowClass = (i === ratings.length - 1) ? 'lastRow' : 'row';
      return (
        <tr class={rowClass}>
          <th>{rating.year}</th>
          <td>
            {image}
            <p>{rating.content}</p>
          </td>
        </tr>
      );
    });

    return (
      <table>
        <caption>{title}</caption>
        <thead>
          <tr>
            <th>Goal</th>
            <td>{goal}</td>
          </tr>
        </thead>
        <tbody>
          <tr class = "row">
            <th>Objective</th>
            <td>{objective}</td>
          </tr>
          <tr class = "row">
            <th>Performance Measure</th>
            <td>{description}</td>
          </tr>
          {rows}
        </tbody>
      </table>
    );
  }
}
