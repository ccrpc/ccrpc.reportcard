# Performance Measure Table

This component renders the table for an individual performance measure. It
retrieves information about the report card, goal, performance measure, and
annual ratings from the report card API.

## API Response
    {
         "id":...,
         "goal":...,
         "title":...,
         "objective":...,
        "description":...,
        "ratings":[
        {rating1},{rating2},{rating3}
        ]
    }

For more information, see `url` in Embed Performance Measure Table section
of the report card editing interface.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                             | Type     | Default     |
| -------- | --------- | --------------------------------------- | -------- | ----------- |
| `url`    | `url`     | The API URL for the performance measure | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
