import { Component, Prop, State, h, getAssetPath } from '@stencil/core';


@Component({
  tag: 'reportcard-summary-table',
  styleUrl: 'summary-table.css',
  shadow: true
})
export class SummaryTable {

  // Indicate that name should be a public property on the component
  @Prop() url: string;
  /**
  *  format: [
                //Goal1: safety and security
                {
  *              "id":...,
  *              "reportcard":...,
  *              "title":...,
  *              "description":...,
                 "performancemeasures":[
 *                   "id":...,
 *                   "goal":...,
 *                   "title":...,
 *                   "objective":...,
 *                   "description":...,
 *                   "ratings":[
 *                   {rating1},{rating2},{rating3}
 *                   ]
                  ]
*             },
            {Goal2},{Goal3}...
          ]
  */
  @State() smData: any;
  @State() years: any[] = [];

  async componentWillLoad() {
    let response = await fetch(this.url);
    this.smData= await response.json();
    this.years = this.getYears();
  }

  //get the length of years
  getYears() {
    for (let goal of this.smData) {
      if (!goal.performancemeasures.length) continue;
      for (let pm of goal.performancemeasures) {
        if (!pm.ratings.length) continue;
        return pm.ratings
          .filter((rating) => rating.rating !== 'baseline')
          .map((rating) => rating.year);
      }
    }
  }

  // get Goal rows
  getGoalRow(goal) {
    let {title, detail_url} = goal;
    let colSpan = this.years.length + 4;
    let pms = goal['performancemeasures'].map(pm => {
      return this.getPerformanceMeasureRow(pm);
    });
    return ([
      <tr>
        <td colSpan={colSpan} class="column-1">
          <a href={detail_url} target="_blank">
          <font size="4" color="white">{title}</font>
          </a>
        </td>
      </tr>,
      pms
    ]);
  }

  getRatingHeader() {
    if (!this.years || !this.years.length) return;
    return (<th class="thead">Ratings</th>);
  }

  getRatingImage(rating) {
    if (!rating) return;
    let imageUrl = getAssetPath(`./rating-image/${rating}.png`);
    return (<img src={imageUrl} width="100" alt={rating}></img>);
  }

  getRatingCells(ratings) {
    if (!this.years || !this.years.length) return;

    let ratingsMap = {};
    ratings.forEach((rating) => ratingsMap[rating.year] = rating.rating);
    let images = this.years.map((year) =>
      <div class="rating-image">
        <span class="year">{year}</span>
        {this.getRatingImage(ratingsMap[year])}
      </div>);
    return (<td>{images}</td>);
  }

  getChart(url) {
    if (!url) return;
    return (
      <rpc-chart
        url={url}
        y-type="linear"
        download={false}
        type="line"
        height="300px"
        width="300px"
        ></rpc-chart>
    );
  }

  //get PerformanceMeasure rows
  getPerformanceMeasureRow(pm) {
    let {title, objective, ratings, detail_url, summary_data_url} = pm;
    return (
      <tr>
        <td>
          <a href={detail_url} target="_blank">{title}</a>
        </td>
        {this.getRatingCells(ratings)}
        <td>
          {this.getChart(summary_data_url)}
        </td>
        <td>
          {ratings.map(rating =>
            <p>
              {rating.content}
            </p>
          )}
        </td>
        <td>
          {objective}
        </td>
      </tr>
    );
  }

  render() {
    //goal category
    let goals = this.smData.map(goal => this.getGoalRow(goal));
    return (
      <div>
        <header>
          <h1>Summary</h1>
        </header>
        <div>
          <h2>Performance Measure Result Scorecard</h2>
          <table>
            <thead>
              <tr>
                <th>
                <font size="4" color="#00544D">Performance<br></br>Measure</font>
                </th>
                {this.getRatingHeader()}
                <th class="thead">Historic<br></br>Trend</th>
                <th class="thead">Current<br></br>Status</th>
                <th class="thead">Plan<br></br>Objective</th>
              </tr>
            </thead>
            <tbody>
              {goals}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
