# summary-page



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default     |
| -------- | --------- | ----------- | -------- | ----------- |
| `url`    | `url`     |             | `string` | `undefined` |


## Dependencies

### Depends on

- rpc-chart

### Graph
```mermaid
graph TD;
  reportcard-summary-table --> rpc-chart
  rpc-chart --> rpc-table
  rpc-chart --> rpc-metadata
  rpc-table --> rpc-metadata
  style reportcard-summary-table fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
