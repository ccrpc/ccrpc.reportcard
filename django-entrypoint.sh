#!/bin/sh

if [ "$DJANGO_DEBUG" != "True" ]; then
  echo "Updating static files..."
  python manage.py collectstatic --no-input
fi

if [ "$POSTGRES_SKIP_CHECK" != "true" ]; then
  echo "Waiting for database to become available..."
  until PGPASSWORD=$POSTGRES_PASSWORD \
      psql -q \
      -h "$POSTGRES_HOST" \
      -U "$POSTGRES_USER" \
      "$POSTGRES_DB" -c '\q' >/dev/null 2>&1; do
    sleep 1
  done
fi

if [ "$DJANGO_DEBUG" != "True" ]; then
  echo "Running migrations..."
  python manage.py migrate
fi

exec "$@"
