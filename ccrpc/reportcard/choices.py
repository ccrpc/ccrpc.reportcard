RATING_STATUS = (
    ('positive', 'Positive'),
    ('neutral', 'Neutral'),
    ('negative', 'Negative'),
    ('baseline', 'Baseline'),
)
