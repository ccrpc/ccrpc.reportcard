"""reportcard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import re_path
from . import views


app_name = 'ccrpc.reportcard'

urlpatterns = [
    re_path(
        r'^(?P<ReportCard_slug>[-\w]+)/summary/$',
        views.summary_detail,
        name='Summary_detail'),
    re_path(
        r'^reportcards/$',
        views.reportcard_list,
        name='ReportCard_list'),
    re_path(
        r'^reportcards/(?P<ReportCard_slug>[-\w]+)/$',
        views.reportcard_detail,
        name='ReportCard_detail'),
    re_path(
        r'^(?P<ReportCard_slug>[-\w]+)/goals/$',
        views.goal_list,
        name='Goal_list'),
    re_path(
        r'^(?P<ReportCard_slug>[-\w]+)/(?P<Goal_slug>[-\w]+)/$',
        views.goal_detail,
        name='Goal_detail'),
    re_path(
        r'^(?P<ReportCard_slug>[-\w]+)/(?P<Goal_slug>[-\w]+)/performancemeasures/$',
        views.performancemeasure_list,
        name='PM_list'),
    re_path(
        r'^(?P<ReportCard_slug>[-\w]+)/(?P<Goal_slug>[-\w]+)/(?P<PM_slug>[-\w]+)/$',
        views.performancemeasure_detail,
        name='PM_detail')
]
