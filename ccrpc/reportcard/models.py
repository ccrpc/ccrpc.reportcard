from django.db import models
from django.urls import reverse
from ccrpc.reportcard.choices import RATING_STATUS


class ReportCard(models.Model):
    embed_code_tag_name = 'reportcard-summary-table'

    title = models.CharField(max_length=200)

    description = models.TextField(max_length=500)

    slug = models.SlugField(null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('reportcard:Summary_detail', kwargs={
                            'ReportCard_slug': self.slug})


# goal
class Goal(models.Model):
    reportcard = models.ForeignKey(ReportCard, on_delete=models.CASCADE)

    title = models.CharField(max_length=200)

    description = models.TextField(max_length=500)

    detail_url = models.CharField(max_length=300, blank=True)

    slug = models.SlugField(null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('reportcard:Goal_detail', kwargs={
                            'ReportCard_slug': self.goal.reportcard.slug,
                            'Goal_slug': self.goal.slug})

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['slug', 'reportcard'], name='unique_goal_slug'),
            ]


# PerformanceMeasure
class PerformanceMeasure(models.Model):
    embed_code_tag_name = 'reportcard-performance-measure-table'

    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)

    title = models.CharField(max_length=300)

    objective = models.CharField(max_length=300)

    description = models.CharField(max_length=300)

    detail_url = models.CharField(max_length=300, blank=True)

    summary_data_url = models.CharField(max_length=300, blank=True)

    slug = models.SlugField(null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('reportcard:PM_detail', kwargs={
                            'ReportCard_slug': self.goal.reportcard.slug,
                            'Goal_slug': self.goal.slug,
                            'PM_slug': self.slug})

    def report_card_name(self):
        return self.goal.reportcard.title

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['slug', 'goal'], name='unique_pm_slug'),
            ]


class Rating(models.Model):
    performance_measure = models.ForeignKey(PerformanceMeasure,
                                            on_delete=models.CASCADE)
    year = models.CharField(max_length=100)

    rating = models.CharField(max_length=100, choices=RATING_STATUS)
    # description
    content = models.TextField(max_length=1000)

    def __str__(self):
        return self.year
