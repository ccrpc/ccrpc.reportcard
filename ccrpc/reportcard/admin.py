from django.contrib import admin
from .models import ReportCard, Goal, PerformanceMeasure, Rating
from django.db import models
from django.forms import TextInput


class RatingInline(admin.TabularInline):
    model = Rating
    extra = 0


class PerformanceMeasureAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['goal']}),
        ('Details',          {'fields': ['title']}),
        (None,               {'fields': ['objective']}),
        (None,               {'fields': ['description']}),
        (None,               {'fields': ['detail_url']}),
        (None,               {'fields': ['summary_data_url']}),
        (None,               {"fields": ['slug']}),
    ]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '200'})}
    }
    list_display = ('title', 'goal', 'report_card_name')
    list_filter = ['goal']
    inlines = [RatingInline]
    prepopulated_fields = {"slug": ("title",)}
    change_form_template = 'reportcard/change_form.html'
    view_on_site = False


class GoalAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['reportcard']}),
        ('Details',          {'fields': ['title']}),
        (None,               {'fields': ['description']}),
        (None,               {'fields': ['detail_url']}),
        (None,               {"fields": ['slug']}),
    ]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '300'})}
    }
    list_display = ('title', 'reportcard')
    prepopulated_fields = {"slug": ("title",)}
    view_on_site = False


class ReportCardAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title']}),
        ('Details', {'fields': ['description']}),
        (None,               {"fields": ['slug']}),
        ]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '300'})}
    }
    change_form_template = 'reportcard/change_form.html'
    view_on_site = False


admin.site.register(ReportCard, ReportCardAdmin)
admin.site.register(Goal, GoalAdmin)
admin.site.register(PerformanceMeasure, PerformanceMeasureAdmin)
