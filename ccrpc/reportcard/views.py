from django.http import HttpResponse, JsonResponse
from django.views import generic
from .models import ReportCard, Goal, PerformanceMeasure
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .serializers import ReportCardSerializer, GoalSerializer, \
    PerformanceMeasureSerializer, RatingSerializer


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def reportcard_list(request):
    if request.method == 'GET':
        reportcards = ReportCard.objects.all()
        serializer = ReportCardSerializer(reportcards, many=True)
        return JSONResponse(serializer.data)


@csrf_exempt
def reportcard_detail(request, ReportCard_slug):
    try:
        reportcard = ReportCard.objects.get(slug=ReportCard_slug)
    except ReportCard.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ReportCardSerializer(reportcard)
        return JSONResponse(serializer.data)


@csrf_exempt
def goal_list(request, ReportCard_slug):
    if request.method == 'GET':
        goals = Goal.objects.filter(
            reportcard__slug=ReportCard_slug)
        serializer = GoalSerializer(goals, many=True)
        return JSONResponse(serializer.data)


@csrf_exempt
def goal_detail(request, ReportCard_slug, Goal_slug):
    try:
        goal = Goal.objects.get(
            slug=Goal_slug,
            reportcard__slug=ReportCard_slug)
    except Goal.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = GoalSerializer(goal)
        return JSONResponse(serializer.data)


@csrf_exempt
def performancemeasure_list(request, ReportCard_slug, Goal_slug):
    if request.method == 'GET':
        performancemeasures = PerformanceMeasure.objects.filter(
            goal__slug=Goal_slug,
            goal__reportcard__slug=ReportCard_slug)
        serializer = PerformanceMeasureSerializer(
                                        performancemeasures, many=True)
        return JSONResponse(serializer.data)


@csrf_exempt
def performancemeasure_detail(request, ReportCard_slug, Goal_slug, PM_slug):
    try:
        performancemeasure = PerformanceMeasure.objects.get(
                                    slug=PM_slug,
                                    goal__slug=Goal_slug,
                                    goal__reportcard__slug=ReportCard_slug)

        ratings = performancemeasure.rating_set.order_by('year')
    except PerformanceMeasure.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer_up = PerformanceMeasureSerializer(performancemeasure)
        rating_list = []
        for rating in ratings:
            serializer_down = RatingSerializer(rating)
            rating_list.append(serializer_down.data)
        data_dic = {
            'id': serializer_up.data['id'],
            'goal': performancemeasure.goal.description,
            'title': serializer_up.data['title'],
            'objective': serializer_up.data['objective'],
            'description': serializer_up.data['description'],
            'detail_url': serializer_up.data['detail_url'],
            'summary_data_url': serializer_up.data['summary_data_url'],
            'ratings': rating_list
        }
        return JsonResponse(data_dic, safe=False)


@csrf_exempt
def summary_detail(request, ReportCard_slug):
    try:
        reportcard = ReportCard.objects.get(slug=ReportCard_slug)
        goals = reportcard.goal_set.order_by('id')
    except Goal.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        summary = []
        data_goals = {}
        for goal in goals:
            goal_serializer = GoalSerializer(goal)
            performancemeasures = goal.performancemeasure_set.order_by('id')
            data_pms = {}
            pm_list = []
            for pm in performancemeasures:
                pm_serializer = PerformanceMeasureSerializer(pm)
                ratings = pm.rating_set.order_by('year')
                rating_list = []
                for rating in ratings:
                    rating_serializer = RatingSerializer(rating)
                    rating_list.append(rating_serializer.data)
                data_pm = {
                    'id': pm_serializer.data['id'],
                    'goal': pm.goal.description,
                    'title': pm_serializer.data['title'],
                    'objective': pm_serializer.data['objective'],
                    'description': pm_serializer.data['description'],
                    'detail_url': pm_serializer.data['detail_url'],
                    'summary_data_url': pm_serializer.data['summary_data_url'],
                    'ratings': rating_list
                }
                data_pms[pm.title] = data_pm
            for value in data_pms.values():
                pm_list.append(value)
            data_goal = {
                'id': goal_serializer.data['id'],
                'reportcard': goal.reportcard.description,
                'title': goal_serializer.data['title'],
                'description': goal_serializer.data['description'],
                'detail_url': goal_serializer.data['detail_url'],
                'performancemeasures': pm_list
            }
            data_goals[goal.title] = data_goal
        for value in data_goals.values():
            summary.append(value)

        return JsonResponse(summary, safe=False)
