from rest_framework import serializers
from .models import ReportCard, Goal, PerformanceMeasure, Rating


class ReportCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportCard
        fields = ('id', 'title', 'description', 'slug')


class GoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Goal
        fields = ('id', 'reportcard', 'title', 'description', 'detail_url', 'slug')


class PerformanceMeasureSerializer(serializers.ModelSerializer):
    class Meta:
        model = PerformanceMeasure
        fields = ('id', 'goal', 'title', 'objective', 'description', 'detail_url', 'summary_data_url', 'slug')


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = (
            'id', 'performance_measure', 'year', 'rating', 'content')
