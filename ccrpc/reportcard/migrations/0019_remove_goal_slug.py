# Generated by Django 2.2.7 on 2019-12-10 17:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reportcard', '0018_auto_20191210_1655'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='goal',
            name='slug',
        ),
    ]
