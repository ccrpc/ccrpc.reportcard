# Generated by Django 2.2.7 on 2019-12-10 17:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reportcard', '0020_goal_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='performancemeasure',
            name='slug',
            field=models.SlugField(null=True),
        ),
        migrations.AddField(
            model_name='reportcard',
            name='slug',
            field=models.SlugField(null=True),
        ),
    ]
