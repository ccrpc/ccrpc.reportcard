from django.apps import AppConfig


class CCRPCReportCardAppConfig(AppConfig):
    """Configure App."""

    name = 'ccrpc.reportcard'
    verbose_name = 'Report Card'
