# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.12](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.11...v0.1.12) (2021-11-01)


### Features

* **summary-table:** add alt text to rating images ([9a583cd](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/9a583cd69538e291913841efb348f6584115f2ab))


### Bug Fixes

* **summary-table:** place ratings in one column and limit chart size ([992be9b](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/992be9b468fbcface7e3e439196a4a552898a391))

### [0.1.11](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.10...v0.1.11) (2021-05-25)


### Features

* **entrypoint:** update static files and run migrations on start ([cccdc9c](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/cccdc9ca78cdeb2d4408b87374b77fd387d7bc3e))
* add baseline option to ratings ([c62ec1a](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/c62ec1aacf33d9e6d27c9fdc217e113654bebc66)), closes [#5](https://gitlab.com/ccrpc/ccrpc.reportcard/issues/5)

### [0.1.10](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.9...v0.1.10) (2020-06-09)


### Bug Fixes

* **reportcard-views:** add parent_slug constraints in views.py when using objects.filter() / .get() ([c3216d9](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/c3216d9203828320350eed265a9e3ed60076d8b8))

### [0.1.9](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.8...v0.1.9) (2020-06-08)


### Bug Fixes

* **migrations:** remove migration for old unique constraints ([04d744f](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/04d744f5fec3d9d5dc2401418356d82e26cdd0c9))
* **reportcard-models:** add unique_constraints to PerformanceMeasure class and Goal class ([ccf05bc](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/ccf05bcfe7ebc0326200f0f31e4b4cb10cd44a51))
* **summary-table:** replace the last column's tile for summary-table to "Plan" rather than "LRTP" ([552eccb](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/552eccb60b0ccab39ee048a24fa507a681a68f43))

### [0.1.8](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.7...v0.1.8) (2020-06-05)


### Bug Fixes

* **reportcard-models:** add unique attributes for each slug fields ([c6f58ed](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/c6f58ed419343803777b49f204d1bcfa9c1c0510))

### [0.1.7](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.6...v0.1.7) (2020-06-03)


### Bug Fixes

* update CCRPC Charts to fix row/column filtering ([f7deac4](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/f7deac426f422f7fc068e489bc449c1ae3f58fd5))

### [0.1.6](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.5...v0.1.6) (2020-06-03)


### Bug Fixes

* use summary URL for report card embed code ([eac2077](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/eac2077746f531f3d3698dc89d48fd223cc3f70d))

### [0.1.5](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.4...v0.1.5) (2020-06-03)


### Features

* **admin:** display WordPress shortcodes ([df32cc5](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/df32cc5612182c20d622c72572bdcf5b2a72cab6))
* upgrade CCRPC Charts for more forgiving URL logic ([6dee3fe](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/6dee3fe8d2c87c2cbedb4337f76778d943d74322))


### Bug Fixes

* **admin:** hide view site links ([a9c3c60](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/a9c3c60aae95bf33519f195523effb51b21eff9f))
* **performance-measure-table:** fix rating image aspect ratio ([8ee8b76](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/8ee8b76ef9b9f6112559bd14bdf85f6cace739ed))
* **summary-table:** check for summary data URL before adding chart ([d24757a](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/d24757a8f3735bf9e9bfcf28085d0c8a73b04008))
* don't set default values for URLs ([89d937f](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/89d937f4a2e63685946e12ea027adaa36d032210))

### [0.1.4](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.3...v0.1.4) (2020-06-02)


### Features

* **summary-page:** add detail_url && summary_data_url to parse pm_table url and csv file for charts ([81d2cbf](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/81d2cbf04e650610984f33329f21c0202ea91169))
* **summary-page:** finished rpc-charts components ([462f7fc](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/462f7fc9795f6d2f706db0583c47a1ad36549912))
* **summary-page component:** add detail_url attribute to Goal field, link a summary_url to rpcchart ([6f5b93a](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/6f5b93ab60657a88feb874b1bc5f8f5e17c9bbf0))
* **summary-table component:** remove static csv file, link summary_url to rpc_chart and fix tagname ([007d84f](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/007d84f750ea57d05443866a3e7798b7d98042bc))


### Bug Fixes

* **performance-measure-table:** add conventional commits package and fix some issues of compotents ([03a57de](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/03a57de63d850689eace9cc8fc9e538698ae29c1))
* **summary-page:** fixed aspect ratio of rating images and replaced hard-code years ([1130892](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/11308925c0ca687fb30ecc19f8eba3eaf5b9eca2))
* **summary-page:** fixed rating issue, left blank if no rating ([f1acf5a](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/f1acf5a65e2eb4faa9bd62663fe3266ec21efb18))
* **summary-page:** reconstructure the summary-page component ([f2a6028](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/f2a602826128c88dc7d8c90cd333455bed5b727e))

### [0.1.3](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.2...v0.1.3) (2020-05-06)


### Bug Fixes

* **performance-measure-table:** bundle images with component ([6837c14](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/6837c14c0f1737290886ec3b07cd815df9d7a7be))

### [0.1.2](https://gitlab.com/ccrpc/ccrpc.reportcard/compare/v0.1.1...v0.1.2) (2020-05-06)


### Features

* use header to determine protocol ([1ab37f2](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/1ab37f2cfd8176e283886f043c670c9f38abeb75))


### Bug Fixes

* disable Django CORS handling in production ([d4dcb45](https://gitlab.com/ccrpc/ccrpc.reportcard/commit/d4dcb45882e20d49749b381c85dad49293dff618))

### 0.1.1 (2020-05-06)

## [0.1.0] - Unreleased
