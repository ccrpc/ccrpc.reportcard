FROM node:alpine
RUN mkdir /code
WORKDIR /code
COPY package.json yarn.lock /code/
RUN yarn install
COPY src /code/src
COPY stencil.config.ts tsconfig.json /code/
RUN npm run build

FROM python:3-alpine
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN apk add --no-cache postgresql-client

RUN apk add --no-cache --virtual .build-deps \
  alpine-sdk \
  gcc \
  python3-dev \
  musl-dev \
  postgresql-dev \
  && pip install --no-cache-dir -r requirements.txt \
  && apk del --no-cache .build-deps
COPY ccrpc /code/ccrpc/
COPY django-entrypoint.sh manage.py /code/
COPY --from=0 /code/dist /code/ccrpc/reportcard/static/reportcard/components

ENTRYPOINT ["/code/django-entrypoint.sh"]
CMD gunicorn -b :8000 ccrpc.site.wsgi
