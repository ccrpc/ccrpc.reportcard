# Report Card

## Credits
Report Card was developed by Wenhao Gu for the
[Champaign County Regional Planning Commission][1].

## License
Report Card is available under the terms of the [BSD 3-clause license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/report-card/blob/master/LICENSE.md
